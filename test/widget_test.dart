// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:developer';

import 'package:bloc_api/repository/product_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';

void main() {
  final getIt = GetIt.instance;

  test('api test', () async {
    // register singleton
    getIt.registerSingleton<Dio>(Dio());
    getIt.registerSingleton<ProductRepository>(ProductRepository());

    final result = await getIt.get<ProductRepository>().getAll();
    for (var element in result) {
      log(element.images.toString());
    }
  });
}
