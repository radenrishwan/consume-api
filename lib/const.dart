String dummyDescription = """
Up for sale is a Space Gray 13-inch MacBook Pro with Touch Bar in gently used condition. This MacBook was purchased new from Apple in May 2019. It has been well taken care of and has no major damage.

Specifications:

13.3 inch Retina display
Intel Core i5 2.4GHz processor (8th generation)
8GB RAM
512GB SSD storage
Touch Bar and Touch ID
4 Thunderbolt 3 ports
Backlit keyboard
Force Touch trackpad
720p FaceTime HD camera
This MacBook is in very good shape - no deep scratches, dents, or cracks anywhere on the body. The screen is in pristine shape with no dead pixels or marks. The keyboard has no stuck or unresponsive keys. The battery holds charge well.

The laptop will be wiped clean and unlocked before shipping out. It comes factory reset with no accounts left on the device. All original accessories are included - charger, USB-C cable, and packaging.

This particular model has served me well over the past few years of grad school. I am selling because I finally upgraded to the new M1 Pro model. Take advantage of the opportunity to own this 2019 MacBook Pro with Touch Bar at a discounted price! It has plenty of life left in it for your everyday needs.

Please inspect the photos closely and let me know if you have any other questions. I have done my best to capture the condition accurately. Don't miss out on this excellent deal for a like-new 13-inch MacBook Pro!
""";
