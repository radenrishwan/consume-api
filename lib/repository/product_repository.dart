import '../model/product.dart';
import 'package:dio/dio.dart';

import '../main.dart';

const _endpoint = "https://dummyjson.com/products";

class ProductRepository {
  Future<List<Product>> getAll({required int limit, required int skip}) async {
    late final List<Product> products = [];
    final response =
        await getIt<Dio>().get("$_endpoint?limit=$limit&skip=$skip");
    final result = response.data;

    for (var element in (result["products"] as List)) {
      products.add(Product.fromJson(element));
    }

    return products;
  }

  Future<Product> getById(String id) async {
    final response = await getIt<Dio>().get("$_endpoint/$id");
    final result = response.data;
    final product = Product.fromJson(result);

    return product;
  }
}
