import 'dart:developer';

import 'package:bloc/bloc.dart';
import '../../main.dart';
import '../../model/product.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../repository/product_repository.dart';

part 'product_event.dart';
part 'product_state.dart';
part 'product_bloc.freezed.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductBloc() : super(const Initial()) {
    final productRepository = getIt<ProductRepository>();

    on<ProductEvent>((event, emit) async {
      if (state is Initial) {
        emit(const ProductState.loading());

        await productRepository.getAll(limit: 10, skip: 0).then((products) {
          emit(ProductState.loaded(products));
        }).catchError((e) {
          emit(ProductState.error(e.toString()));
        });
      }

      if (state is Loaded) {
        final data = (state as Loaded).products;

        emit(await event.when(
          started: () async {
            return ProductState.loaded(data);
          },
          loadMore: (skip, limit) async {
            log("event type : ${event.runtimeType}");
            emit(const ProductState.loading());

            try {
              final result = await productRepository.getAll(
                  limit: int.parse(limit), skip: int.parse(limit));

              return ProductState.loaded([...data, ...result]);
            } catch (error) {
              return ProductState.error(error.toString());
            }
          },
        ));
      }
    });
  }
}
