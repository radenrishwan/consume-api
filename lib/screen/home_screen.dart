import 'dart:developer';

import 'detail_screen.dart';

import '../bloc/bloc/product_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';

import '../model/product.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var skip = 10;
    var lastScrollPosition = 0.0;
    final ScrollController scrollController = ScrollController();

    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        lastScrollPosition = scrollController.position.maxScrollExtent;
        log("lastScrollPosition : $lastScrollPosition");
        context.read<ProductBloc>().add(
              ProductEvent.loadMore(
                5.toString(),
                (skip += 5).toString(),
              ),
            );
      }
    });

    return Scaffold(
      appBar: AppBar(
        title: const Text('Lesson 1'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: BlocConsumer<ProductBloc, ProductState>(
          listener: (context, state) {
            state.whenOrNull(
              error: (error) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(error),
                  ),
                );
              },
              loading: () {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text("Loading..."),
                  ),
                );
              },
            );
          },
          builder: (context, state) {
            log("state : ${state.runtimeType}");
            if (state is Initial) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            final data = state.whenOrNull<List<Product>>(
                  loaded: (data) => data,
                ) ??
                [];

            return ListView.separated(
              controller: scrollController,
              physics: const AlwaysScrollableScrollPhysics(),
              separatorBuilder: (context, index) {
                return const SizedBox(height: 8);
              },
              itemCount: data.length,
              itemBuilder: (context, index) {
                final product = data[index];
                return card(context, product);
              },
            );
          },
        ),
      ),
    );
  }

  Widget card(BuildContext context, Product product) {
    return CachedNetworkImage(
      imageUrl: product.thumbnail,
      placeholder: (context, url) {
        return SizedBox(
          width: double.infinity,
          height: 200,
          child: Shimmer.fromColors(
            baseColor: Colors.white,
            highlightColor: Colors.blue,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white,
              ),
            ),
          ),
        );
      },
      imageBuilder: (context, imageProvider) {
        return InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return DetailScreen(
                  id: product.id.toString(), title: product.title);
            }));
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  width: double.infinity,
                  height: 200,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.8),
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(8),
                      bottomRight: Radius.circular(8),
                    ),
                  ),
                  width: double.infinity,
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.title,
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      Text(
                        product.description.length < 75
                            ? product.description
                            : "${product.description.substring(0, 75)} ...",
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
