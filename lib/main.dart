import 'screen/home_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc/product_bloc.dart';
import 'repository/product_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

GetIt getIt = GetIt.instance;

void main() {
  getIt.registerSingleton<Dio>(Dio());
  getIt.registerSingleton<ProductRepository>(ProductRepository());

  runApp(const InitialApp());
}

class InitialApp extends StatelessWidget {
  const InitialApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider(
        create: (context) => ProductBloc()..add(const ProductEvent.started()),
        child: const HomeScreen(),
      ),
    );
  }
}
