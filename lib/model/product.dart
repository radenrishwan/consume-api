import 'package:freezed_annotation/freezed_annotation.dart';

part 'product.freezed.dart';
part 'product.g.dart';

@freezed
class Product with _$Product {
  const factory Product({
    required int id,
    required String title,
    required String description,
    required int price,
    required double discountPercentage,
    required double rating,
    required int stock,
    required String brand,
    required String category,
    required String thumbnail,
    required List<String> images,
  }) = _Product;

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
}

//  "id": 1,
//  "title": "iPhone 9",
//  "description": "An apple mobile which is nothing like apple",
//  "price": 549,
//  "discountPercentage": 12.96,
//  "rating": 4.69,
//  "stock": 94,
//  "brand": "Apple",
//  "category": "smartphones",
//  "thumbnail": "...",
//  "images": ["...", "...", "..."]